# Mensa Registration App

This app provides functionality to send registration data to the Corona contact tracing website from the Studentenwerk Leipzig.

## Download

The latest development version is [available for download](https://gitlab.com/michael.becker/mensa-app/-/packages/815394).