/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2020, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package de.caterdev.mensalogin.model

import de.caterdev.mensalogin.PARAM_VIEWSTATE
import org.jsoup.Connection

/**
 * Adds the data from the given connection data to the connection.
 *
 * As a result, the connection headers and cookies are fully set and
 * the connections contains the data PARAM_VIEWSTATE.
 *
 * @param sessionData the connection data to add
 *
 * @return this connection, for chaining
 */
fun Connection.init(sessionData: SessionData, partialRequest: Boolean = false): Connection {
    header("Host", "buergerbeteiligung.sachsen.de")
    header("Accept", "application/xml, text/xml, */*; q=0.01")
    header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
    header("Connection", "keep-alive")
    if (partialRequest) {
        header("Faces-Request", "partial/ajax")
    }
    header("Origin", "https://buergerbeteiligung.sachsen.de")
    header("Referer", "https://buergerbeteiligung.sachsen.de/portal/skc/beteiligung/themen/1022287")
    header("X-Requested-With", "XMLHttpRequest")

    cookie("JSESSIONID", sessionData.sessionId)

    data(PARAM_VIEWSTATE, sessionData.viewState)

    return this
}