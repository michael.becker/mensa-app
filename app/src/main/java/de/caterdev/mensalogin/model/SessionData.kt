/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2020, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package de.caterdev.mensalogin.model

import de.caterdev.mensalogin.ID_SESSION
import de.caterdev.mensalogin.ID_VIEWSTATE
import org.jsoup.Connection
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element

/**
 * Holds the data necessary for a single registration session.
 *
 * @param response the initial response of this registration
 */
class SessionData(private val response: Connection.Response) {
    private val document = response.parse()
    val facilityTokens = parseFacilityTokens(document)
    val householdTokens = parseHouseholdTokens(document)
    val contactTypeTokens = parseContactTypeTokens(document)
    val sessionId: String = response.cookie(ID_SESSION)
    val viewState: String = getAttributeValue(document, ID_VIEWSTATE)

    /**
     * Parses the session Ids of existing facilities.
     */
    private fun parseFacilityTokens(document: Document): Map<Facility, String> {
        val result = HashMap<Facility, String>(Facility.values().size)

        Facility.values().forEach { facility ->
            result[facility] = getAttributeValue(document, facility.formId)
        }

        return result
    }

    /**
     * Parses the session Ids of existing household types.
     */
    private fun parseHouseholdTokens(document: Document): Map<Household, String> {
        val result = HashMap<Household, String>(Household.values().size)

        Household.values().forEach { household ->
            result[household] = getAttributeValue(document, household.formId)
        }

        return result
    }

    /**
     * Parses the session Ids of existing contact types.
     */
    private fun parseContactTypeTokens(document: Document): Map<ContactType, String> {
        val result = HashMap<ContactType, String>(ContactType.values().size)

        ContactType.values().forEach { contactType ->
            result[contactType] = getAttributeValue(document, contactType.formId)
        }

        return result
    }


    /**
     * Parses the tokens of the given response.
     * After parsing is finished, the maps contain the expected tokens.
     *
     * @param response the response to parse
     */
    /*
    fun parseResponseTokens(document: Document) {
        val body = document.body()

        Facility.values().forEach { facility ->
            facilityTokens[facility] = getAttributeValue(body, facility.formId)
        }

        Household.values().forEach { household ->
            householdTokens[household] = getAttributeValue(body, household.formId)
        }

        ContactType.values().forEach { contactType ->
            contactTypeTokens[contactType] = getAttributeValue(body, contactType.formId)
        }
    }

     */

    /**
     * Returns the value of the attribute <code>value</code> from the element with the given Id.
     *
     * @param body the body element
     * @param id the id to parse the attribute value for
     * @return the attribute value of the element with the given Id
     */
    private fun getAttributeValue(body: Element, id: String): String {
        return body.getElementById(id).attr("value")
    }
}