/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2020, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package de.caterdev.mensalogin.model

class Registration(
    val firstName: String,
    val lastName: String,
    val zipCode: String,
    val tableNumber: String,
    val facility: Facility,
    val household: Household,
    val contactType: ContactType,
    val contact: String
) {
    override fun toString(): String {
        return "$firstName $lastName $zipCode $tableNumber $facility $household $contactType $contact"
    }
}

enum class Input(val formId: String, val formName: String, val label: String) {
    FirstName(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:3:gruppen:0:unterelemente:0:unterelement:frage:inputText",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:3:gruppen:0:unterelemente:0:unterelement:frage:inputText",
        "Vorname"
    ),
    LastName(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:3:gruppen:0:unterelemente:1:unterelement:frage:inputText",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:3:gruppen:0:unterelemente:1:unterelement:frage:inputText",
        "Nachname"
    ),
    ZipCode(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:4:gruppen:0:unterelemente:0:unterelement:frage:inputText",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:4:gruppen:0:unterelemente:0:unterelement:frage:inputText",
        "Postleitzahl"
    ),
    TableNumber(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:1:unterelement:frage:inputText",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:1:unterelement:frage:inputText",
        "Tischnummer"
    ),

    Phone(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:6:gruppen:0:unterelemente:0:unterelement:frage:inputText",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:6:gruppen:0:unterelemente:0:unterelement:frage:inputText",
        "Telefonnummer"
    ),
    Mail(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:7:gruppen:0:unterelemente:0:unterelement:frage:inputText",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:7:gruppen:0:unterelemente:0:unterelement:frage:inputText",
        "E-Mail-Adresse"
    ),
}

enum class Facility(val formId: String, val formName: String, val label: String) {
    MensaAmPark(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:einfach_options:0:einfach_option",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:selectOneRadio",
        "Mensa am Park"
    ),
    MensaPeterssteinweg(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:einfach_options:1:einfach_option",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:selectOneRadio",
        "Mensa Peterssteinweg"
    ),
    MensaAmElsterbecken(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:einfach_options:2:einfach_option",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:selectOneRadio",
        "Mensa am Elsterbecken"
    ),
    CafeElsterbecken(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:einfach_options:3:einfach_option",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:selectOneRadio",
        "Cafeteria am Elsterbecken"
    ),
    MensaAcademica(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:einfach_options:4:einfach_option",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:selectOneRadio",
        "Mensa Academica"
    ),
    MensaMedizin(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:einfach_options:5:einfach_option",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:selectOneRadio",
        "Mensa Medizincampus"
    ),
    MensaTierkliniken(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:einfach_options:6:einfach_option",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:selectOneRadio",
        "Mensa An den Tierkliniken"
    ),
    MensaBotGarten(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:einfach_options:7:einfach_option",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:selectOneRadio",
        "Mensaria am botanischen Garten"
    ),
    MensaSchoenauer(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:einfach_options:8:einfach_option",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:selectOneRadio",
        "Mensa Schönauer Straße"
    ),
    CafeDittrichring(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:einfach_options:9:einfach_option",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:selectOneRadio",
        "Cafeteria Dittrichring"
    ),
    CafeMusikviertel(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:einfach_options:10:einfach_option",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:selectOneRadio",
        "Cafeteria im Musikviertel"
    ),
    CafePark(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:einfach_options:11:einfach_option",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:0:gruppen:0:unterelemente:0:unterelement:frage:selectOneRadio",
        "Cafeteria am Park"
    );
}

enum class Household(val formId: String, val formName: String, val label: String) {
    SinglePerson(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:1:gruppen:0:unterelemente:0:unterelement:frage:einfach_options:0:einfach_option",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:1:gruppen:0:unterelemente:0:unterelement:frage:selectOneRadio",
        "Ja"
    ),
    GroupOneHH(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:1:gruppen:0:unterelemente:0:unterelement:frage:einfach_options:1:einfach_option",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:1:gruppen:0:unterelemente:0:unterelement:frage:selectOneRadio",
        "Nein, als Gruppe, die in einem Hausstand lebt"
    ),
    GroupMultipleHH(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:1:gruppen:0:unterelemente:0:unterelement:frage:einfach_options:2:einfach_option",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:1:gruppen:0:unterelemente:0:unterelement:frage:selectOneRadio",
        "Nein, als Gruppe, die in unterschiedlichen Hausständen lebt"
    );
}

enum class ContactType(val formId: String, val formName: String, val label: String) {
    Phone(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:5:gruppen:0:unterelemente:0:unterelement:frage:einfach_options:0:einfach_option",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:5:gruppen:0:unterelemente:0:unterelement:frage:selectOneRadio",
        "Telefonnummer"
    ),
    Mail(
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:5:gruppen:0:unterelemente:0:unterelement:frage:einfach_options:1:einfach_option",
        "tabForm:fbContainer:fragebogen:fb-seiten:seitenelemente:gruppierungen:5:gruppen:0:unterelemente:0:unterelement:frage:selectOneRadio",
        "E-Mail-Adresse"
    );

    fun getContactTextFormName(): String {
        return when (this) {
            Mail -> Input.Mail.formName
            Phone -> Input.Phone.formName
        }
    }
}