/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2020, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package de.caterdev.mensalogin.handler

import android.widget.EditText
import android.widget.Spinner
import de.caterdev.mensalogin.URL
import de.caterdev.mensalogin.log
import de.caterdev.mensalogin.model.*
import org.jsoup.Connection
import org.jsoup.nodes.Document
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class TaskHandler(private val logDir: File, private val pdfDir: File) {
    private val connectionHandler = ConnectionHandler()
    private val responseHandler = PartialResponseHandler()
    private val errorHandler = ErrorHandler()

    /**
     * Reloads existing data from the given file and updates the given UI elements.
     */
    fun reloadExistingData(
        saveFile: File,
        inputFirstName: EditText,
        inputLastName: EditText,
        inputZipCode: EditText,
        inputContact: EditText,
        inputFacility: Spinner,
        inputHousehold: Spinner,
        inputContactType: Spinner
    ) {
        if (saveFile.exists()) {
            val fileInputStream = FileInputStream(saveFile)
            val size = fileInputStream.available()
            val buffer = ByteArray(size)
            fileInputStream.read(buffer)
            val content = String(buffer).split(",")

            if (7 == content.size) {
                inputFirstName.setText(content[0])
                inputLastName.setText(content[1])
                inputZipCode.setText(content[2])
                inputFacility.setSelection(Facility.valueOf(content[3]).ordinal)
                inputHousehold.setSelection(Household.valueOf(content[4]).ordinal)
                inputContactType.setSelection(ContactType.valueOf(content[5]).ordinal)
                inputContact.setText(content[6])
            }
        }
    }

    fun reloadMailIfExists(saveFile: File, inputContact: EditText) {
        if (saveFile.exists()) {
            val fileInputStream = FileInputStream(saveFile)
            val size = fileInputStream.available()
            val buffer = ByteArray(size)
            fileInputStream.read(buffer)
            val content = String(buffer).split(",")

            if (7 == content.size && ContactType.Mail.name == content[5]) {
                inputContact.setText(content[6])
            }
        }
    }

    fun reloadPhoneIfExists(saveFile: File, inputContact: EditText) {
        if (saveFile.exists()) {
            val fileInputStream = FileInputStream(saveFile)
            val size = fileInputStream.available()
            val buffer = ByteArray(size)
            fileInputStream.read(buffer)
            val content = String(buffer).split(",")

            if (7 == content.size && ContactType.Phone.name == content[5]) {
                inputContact.setText(content[6])
            }
        }
    }

    fun prepareRegistration(
        inputFirstName: EditText, inputLastName: EditText, inputZipCode: EditText, inputFacility: Spinner, inputTableNumber: EditText,
        inputHousehold: Spinner, inputContactType: Spinner, inputContact: EditText
    ): Registration {
        val firstName = inputFirstName.text.toString()
        val lastName = inputLastName.text.toString()
        val zipCode = inputZipCode.text.toString()
        val contact = inputContact.text.toString()
        val tableNumber = inputTableNumber.text.toString()

        if (firstName.isBlank()) {
            throw RuntimeException("first name must not be empty")
        }
        if (lastName.isBlank()) {
            throw RuntimeException("last name must not be emtpy")
        }
        if (zipCode.isBlank()) {
            throw RuntimeException("zip code must not be empty")
        }
        if (contact.isBlank()) {
            throw RuntimeException("mail must not be empty")
        }
        if (tableNumber.isEmpty()) {
            throw RuntimeException("table number must not be empty")
        }

        val facility = inputFacility.selectedItem as Facility
        val household = inputHousehold.selectedItem as Household
        val contactType = inputContactType.selectedItem as ContactType

        return Registration(firstName, lastName, zipCode, tableNumber, facility, household, contactType, contact)
    }

    fun saveRegistration(registration: Registration, saveFile: File) {
        val content = "${registration.firstName},${registration.lastName},${registration.zipCode}," +
                "${registration.facility},${registration.household},${registration.contactType},${registration.contact}"

        FileOutputStream(saveFile).use {
            it.write(content.toByteArray())
        }
    }

    /**
     * Registers with the given data
     *
     * @param registration the registration data to register with
     * @return the registration evaluated from the reponse data
     */
    fun callPage(registration: Registration): Registration {
        val responseInit = connectionHandler.prepareInitConnection(URL).execute()

        if (!validateResponseDocument(responseInit.bufferUp().parse())) {
            throw RuntimeException("Response document does not contain expected data!")
        }

        log(responseInit.bufferUp().parse().toString(), logDir, "initial-response.html")

        val sessionData = SessionData(responseInit)

        val connectionContactType = connectionHandler.prepareRegisterContactType(URL, registration.contactType, sessionData)
        log(getLogValueFromPostData(connectionContactType), logDir, "select-contact-post.txt")

        val responseContactType = connectionContactType.execute()
        log(responseContactType.parse().toString(), logDir, "select-contact-response.xml")

        val connectionRegistration = connectionHandler.prepareSendRegistration(URL, registration, sessionData)
        log(getLogValueFromPostData(connectionRegistration), logDir, "register-post.txt")

        val response = connectionRegistration.execute().parse()

        val responseContent = response.toString()
        val responseDocument = responseHandler.evaluateDocument(responseContent)
        log(responseContent, logDir, "register-response.xml")

        if (!responseHandler.isPartialResponse(responseContent)) {
            throw RuntimeException("Unsupported result. See log for details.")
        } else if (errorHandler.hasError(responseContent)) {
            throw RuntimeException(
                "fields in error: ${errorHandler.getErrorFields(response)}. " +
                        "See log files for additional details"
            )
        }

        val connectionDownload = connectionHandler.prepareDownloadPDF(URL, sessionData, responseDocument)
        log(getLogValueFromPostData(connectionDownload), logDir, "download-post.txt")
        val responseDownload = connectionDownload.ignoreContentType(true).execute()
        log(responseDownload.bodyAsBytes(), logDir, "download-response.txt")
        log(responseDownload.bodyAsBytes(), pdfDir, "registration.pdf")

        return responseHandler.evaluateRegistration(responseDocument)
    }

    private fun getLogValueFromPostData(connection: Connection): String {
        val result = StringBuilder()
        result.append("request cookies")
        connection.request().cookies().forEach { result.append("\t$it\n") }
        result.append("request header")
        connection.request().headers().forEach { result.append("\t$it\n") }
        result.append("request data")
        connection.request().data().forEach { result.append("\t$it\n") }

        return result.toString()
    }


    /**
     * Validates that a given HTML response document contains expected data fields by comparing
     * label values.
     *
     * @param document the document to parse
     * @return true, iff the document contains all expected data fields, otherwise false
     */
    private fun validateResponseDocument(document: Document): Boolean {
        Input.values().forEach { input ->
            // mail and phone input fields are not in the original HTML page
            if (input != Input.Mail && input != Input.Phone) {
                if (getLabelValue(document, input.formId) != input.label) {
                    return false
                }
            }
        }

        Facility.values().forEach { facility ->
            if (getLabelValue(document, facility.formId) != facility.label) {
                return false
            }
        }

        Household.values().forEach { household ->
            if (getLabelValue(document, household.formId) != household.label) {
                return false
            }
        }

        ContactType.values().forEach { contactType ->
            if (getLabelValue(document, contactType.formId) != contactType.label) {
                return false
            }
        }

        return true
    }


    private fun getLabelValue(document: Document, formName: String): String {
        return document.getElementsByAttributeValue("for", formName)[0].text()
    }
}