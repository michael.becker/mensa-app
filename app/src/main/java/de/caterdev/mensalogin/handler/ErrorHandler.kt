/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2020, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package de.caterdev.mensalogin.handler

import de.caterdev.mensalogin.model.ContactType
import de.caterdev.mensalogin.model.Facility
import de.caterdev.mensalogin.model.Household
import de.caterdev.mensalogin.model.Input
import org.jsoup.nodes.Document

/**
 * This class provides methods to evaluate responses from the server and to identify possible
 * field errors.
 */
class ErrorHandler {
    private val responseHandler = PartialResponseHandler()

    /**
     * Returns true if the given response string contains errors identified by presence of elements
     * with attribute <code>aria-invalide</code>
     *
     * @param response the response string to parse
     * @return true, iff the response string has errors
     */
    fun hasError(response: String): Boolean {
        return response.contains("aria-invalid=\"true\"")
    }

    /**
     * Returns a list of field Ids that contain errors for the given response string.
     * The field values are reflected by entries in formConstants#FIELD_MAPPINGS.
     *
     * @param response the response string to parse
     * @return a list of fields in error for the given response string
     */
    fun getErrorFields(document: Document): Collection<String> {
        val elements = responseHandler.evaluateDocument(document.toString()).getElementsByAttributeValue("aria-invalid", "true")
        val result = mutableListOf<String>()
        val formIds = mutableMapOf<String, String>()

        Facility.values().forEach { formIds[it.formId] = it.name }
        Household.values().forEach { formIds[it.formId] = it.name }
        ContactType.values().forEach { formIds[it.formId] = it.name }
        Input.values().forEach { formIds[it.formId] = it.name }

        elements.forEach { element ->
            val id = element.id()

            if (!formIds.contains(id)) {
                throw RuntimeException("unknown field id $id")
            }

            result.add(formIds[id]!!)
        }

        return result
    }


}