/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2020, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package de.caterdev.mensalogin.handler

import de.caterdev.mensalogin.model.*
import org.jsoup.Connection
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

/**
 * Provides methods for connecting with the portal server and sending/receiving registration data.
 */
class ConnectionHandler {
    private val responseHandler = PartialResponseHandler()

    /**
     * Initialises the connection and returns the connection data consisting of session Id,
     * view state, and the form Ids for registrations.
     *
     * @param url the URL to connect to
     * @return the  data associated with this connection
     */
    fun prepareInitConnection(url: String): Connection {
        return Jsoup.connect(url).method(Connection.Method.GET)
    }

    /**
     * Prepares a connection to register the given contact type.
     * This method must be called before the final values are sent to update the server settings.
     *
     * @param url the URL to connect to
     * @param contactType the contact type to register with
     * @param sessionData the data for the current session
     * @return the prepared connection ready for execution
     */
    fun prepareRegisterContactType(url: String, contactType: ContactType, sessionData: SessionData): Connection {
        val connection = Jsoup.connect(url).init(sessionData)

        connection.data("javax.faces.source", "tabForm:fbContainer:fragebogen:fb-seiten:nbp_rc_update_fb")
        connection.data("javax.faces.partial.execute", "tabForm")
        connection.data("javax.faces.partial.render", "tabForm")
        connection.data("tabForm:fbContainer:fragebogen:fb-seiten:nbp_rc_update_fb", "tabForm:fbContainer:fragebogen:fb-seiten:nbp_rc_update_fb")
        connection.data("tabForm", "tabForm")

        connection.data(contactType.formName, sessionData.contactTypeTokens[contactType])

        return connection.method(Connection.Method.POST)
    }

    /**
     * Prepares a connection to send registration data.
     *
     * @param url the URL to connect to
     * @param registration the registration to send
     * @param sessionData the data for the current session
     * @return the prepared connection ready for execution
     */
    fun prepareSendRegistration(url: String, registration: Registration, sessionData: SessionData): Connection {
        val connection = Jsoup.connect(url).init(sessionData)

        // add registration specific data
        connection.data(Input.FirstName.formName, registration.firstName)
        connection.data(Input.LastName.formName, registration.lastName)
        connection.data(Input.ZipCode.formName, registration.zipCode)
        connection.data(Input.TableNumber.formName, registration.tableNumber)
        connection.data(registration.facility.formName, sessionData.facilityTokens[registration.facility])
        connection.data(registration.contactType.formName, sessionData.contactTypeTokens[registration.contactType])
        connection.data(registration.contactType.getContactTextFormName(), registration.contact)
        connection.data(registration.household.formName, sessionData.householdTokens[registration.household])

        // add data necessary for submitting
        connection.data("javax.faces.partial.ajax", "true")
        connection.data("javax.faces.source", "tabForm:fbContainer:fragebogen:senden")
        connection.data("javax.faces.partial.execute", "@all")
        connection.data("javax.faces.partial.render", "tabForm statusUpdate")
        connection.data("tabForm:fbContainer:fragebogen:senden", "tabForm:fbContainer:fragebogen:senden")
        connection.data("tabForm", "tabForm")

        return connection.method(Connection.Method.POST)
    }

    /**
     * Prepares a connection to download a registration.
     *
     * @param url the URL to connect to
     * @param sessionData the data for the current session
     * @return the prepared connection ready for execution
     */
    fun prepareDownloadPDF(url: String, sessionData: SessionData, responseRegistration: Document): Connection {
        val connection = Jsoup.connect(url).init(sessionData, partialRequest = false)
        val downloadToken = responseHandler.parseForDownloadToken(responseRegistration)

        connection.data(downloadToken, downloadToken)
        connection.data("tabForm", "tabForm")

        return connection.method(Connection.Method.POST)
    }


}



