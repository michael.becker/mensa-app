/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2020, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package de.caterdev.mensalogin.handler

import de.caterdev.mensalogin.model.*
import org.jsoup.helper.DataUtil
import org.jsoup.nodes.Document
import java.io.ByteArrayInputStream
import java.nio.charset.Charset

class PartialResponseHandler {
    /**
     * Evaluates whether the given response text is a partial response
     *
     * @param response the response to evaluate
     * @return true, iff the given response text is a partial response
     */
    fun isPartialResponse(response: String): Boolean {
        val doubleQuotationMarks = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
        val singleQuotationMarks = "<?xml version='1.0' encoding='UTF-8'?>"

        return (response.startsWith(doubleQuotationMarks) || response.startsWith(singleQuotationMarks)) &&
                response.contains("<partial-response>")
    }

    /**
     * Parses the given response string into a Document using the Base URI <code>baseUri</code>
     *
     * @param response the response from a connect request
     * @return the document with the given response as body
     */
    fun evaluateDocument(response: String): Document {
        ByteArrayInputStream(parseResponseDocument(response).toByteArray()).use { inputStream ->
            return DataUtil.load(inputStream, Charset.defaultCharset().name(), "baseUri")
        }
    }

    /**
     * Parses the given response string and searches for the content between
     * <code>&lt;!&#91;CDATA&#91;</code> and <code>&#93;&#93;&gt;</code>.
     *
     * @param response the response from a connect request
     * @return the content between <code>&lt;!&#91;CDATA&#91;</code> and <code>&#93;&#93;&gt;&lt;/update&gt;</code>
     */
    fun parseResponseDocument(response: String): String {
        val beginResponseText = response.indexOf("<![CDATA[") + "<![CDATA[".length
        val endResponseText = response.indexOf("]]>")

        return response.substring(beginResponseText, endResponseText).trim()
    }

    /**
     * Parses the given response string and creates a registration from it.
     *
     * @param document the response to parse
     * @return the registration represented by the given response
     */
    fun evaluateRegistration(document: Document): Registration {
        val firstName = document.getElementById(Input.FirstName.formId).attr("value")
        val lastName = document.getElementById(Input.LastName.formId).attr("value")
        val zipCode = document.getElementById(Input.ZipCode.formId).attr("value")
        val tableNumber = document.getElementById(Input.TableNumber.formId).attr("value")
        val contact = document.getElementById(Input.Mail.formId).attr("value")


        return Registration(
            firstName,
            lastName,
            zipCode,
            tableNumber,
            evaluateSelectedFacility(document),
            evaluateSelectedHousehold(document),
            evaluateSelectedContactType(document),
            contact
        )
    }

    /**
     * Parses the given document for the PDF download token.
     */
    fun parseForDownloadToken(document: Document): String {
        val result = document.getElementsByAttributeValueStarting("id", "tabForm:fbContainer:fragebogen:j")

        return result.attr("id")
    }

    private fun evaluateSelectedFacility(document: Document): Facility {
        Facility.values().forEach { facility ->
            if (document.getElementById(facility.formId).hasAttr("checked")) {
                return facility
            }
        }

        throw RuntimeException("No selected facility found!")
    }

    private fun evaluateSelectedHousehold(document: Document): Household {
        Household.values().forEach { household ->
            if (document.getElementById(household.formId).hasAttr("checked")) {
                return household
            }
        }

        throw RuntimeException("No selected household found!")
    }

    private fun evaluateSelectedContactType(document: Document): ContactType {
        ContactType.values().forEach { contactType ->
            if (document.getElementById(contactType.formId).hasAttr("checked")) {
                return contactType
            }
        }

        throw RuntimeException("No selected contact type found!")
    }
}