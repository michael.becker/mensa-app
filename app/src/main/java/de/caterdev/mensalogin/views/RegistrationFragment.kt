/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2020, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package de.caterdev.mensalogin.views

import android.graphics.Color
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import de.caterdev.mensalogin.R
import de.caterdev.mensalogin.handler.TaskHandler
import de.caterdev.mensalogin.log
import de.caterdev.mensalogin.model.ContactType
import de.caterdev.mensalogin.model.Facility
import de.caterdev.mensalogin.model.Household
import de.caterdev.mensalogin.model.Registration
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File

class RegistrationFragment(private val saveFile: File, private val logDir: File, pdfDir: File) : Fragment() {
    private val taskHandler = TaskHandler(logDir, pdfDir)

    private lateinit var inputFirstName: EditText
    private lateinit var inputLastName: EditText
    private lateinit var inputZipCode: EditText
    private lateinit var inputFacility: Spinner
    private lateinit var inputHousehold: Spinner
    private lateinit var inputContactType: Spinner
    private lateinit var inputContact: EditText
    private lateinit var inputTableNumber: EditText

    private lateinit var submitButton: Button

    private lateinit var messageBox: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_registration, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        inputFirstName = view.findViewById(R.id.inputFirstname)
        inputLastName = view.findViewById(R.id.inputLastName)
        inputZipCode = view.findViewById(R.id.inputZipCode)
        inputFacility = view.findViewById(R.id.inputFacility)
        inputHousehold = view.findViewById(R.id.inputHousehold)
        inputContactType = view.findViewById(R.id.inputContactType)
        inputContact = view.findViewById(R.id.inputContact)
        inputTableNumber = view.findViewById(R.id.inputTableNumber)

        messageBox = view.findViewById(R.id.messages)

        submitButton = view.findViewById(R.id.buttonSend)

        inputFacility.adapter = ResourceArrayAdapter(view, R.array.facility, Facility.values())
        inputHousehold.adapter = ResourceArrayAdapter(view, R.array.household, Household.values())
        inputContactType.adapter = ResourceArrayAdapter(view, R.array.contact_type, ContactType.values())

        reloadExistingData()

        inputContactType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                println("clicked on position $position")
                inputContact.text.clear()

                if (0 == position) {
                    inputContact.inputType = InputType.TYPE_CLASS_PHONE
                    inputContact.hint = getString(R.string.phone)
                    taskHandler.reloadPhoneIfExists(saveFile, inputContact)
                } else {
                    inputContact.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                    inputContact.hint = getString(R.string.email)
                    taskHandler.reloadMailIfExists(saveFile, inputContact)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }


        submitButton.setOnClickListener {
            if (!validateInputs()) {
                messageBox.setText(R.string.fields_empty)
                return@setOnClickListener
            }

            try {
                val registration = taskHandler.prepareRegistration(
                    inputFirstName, inputLastName, inputZipCode,
                    inputFacility, inputTableNumber, inputHousehold,
                    inputContactType, inputContact
                )
                taskHandler.saveRegistration(registration, saveFile)
                messageBox.text = ""
                inputTableNumber.text.clear()

                submitButton.isEnabled = false

                GlobalScope.launch(Dispatchers.IO) {
                    sendData(registration)
                }
            } catch (e: Exception) {
                displayErrorMessage(e.toString())
            }

        }

    }

    private fun reloadExistingData() {
        taskHandler.reloadExistingData(
            saveFile,
            inputFirstName,
            inputLastName,
            inputZipCode,
            inputContact,
            inputFacility,
            inputHousehold,
            inputContactType
        )
    }

    private fun sendData(registration: Registration) {
        try {
            val receivedRegistration = taskHandler.callPage(registration)
            messageBox.text = "erfolgreich registriert: $receivedRegistration"
        } catch (e: Exception) {
            displayErrorMessage(e.toString())
            log(e.stackTrace.contentDeepToString(), logDir, "exception.txt")
        }
    }

    private fun displayErrorMessage(message: String) {
        messageBox.text = message
    }

    private fun validateInputs(): Boolean {
        var inputValid = true

        if (isBlank(inputFirstName)) {
            inputValid = false
        }
        if (isBlank(inputLastName)) {
            inputValid = false
        }
        if (isBlank(inputZipCode)) {
            inputValid = false
        }
        if (isBlank(inputTableNumber)) {
            inputValid = false
        }
        if (isBlank(inputContact)) {
            inputValid = false
        }

        return inputValid
    }

    private fun isBlank(editText: EditText): Boolean {
        if (editText.text.isBlank()) {
            editText.setBackgroundColor(Color.RED)
            return true
        } else {
            editText.setBackgroundColor(Color.WHITE)
            return false
        }
    }

    private class ResourceArrayAdapter<T>(view: View, strings: Int, values: Array<T>) : ArrayAdapter<T>(view.context, strings, values) {
        private val stringValues = view.resources.getStringArray(strings)

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val view = TextView(parent.context)
            view.text = stringValues[position]
            return view
        }

        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
            val view = TextView(parent.context)
            view.text = stringValues[position]
            return view
        }
    }
}
