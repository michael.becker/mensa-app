package de.caterdev.mensalogin

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import de.caterdev.mensalogin.handler.ConnectionHandler
import de.caterdev.mensalogin.model.*
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.jsoup.Connection
import org.jsoup.helper.DataUtil
import org.jsoup.nodes.Document
import org.junit.jupiter.api.Test
import java.nio.charset.Charset

class ConnectionHandlerTest : MensaAppTest() {
    private val connectionUtil = ConnectionHandler()
    private val document: Document
        get() {
            return DataUtil.load(getInitialResponse(), Charset.defaultCharset().name(), "baseUri")
        }
    private val response = mock<Connection.Response> {
        on { cookie(ID_SESSION) } doReturn "testSession"
        on { parse() } doReturn document
    }
    private val sessionData = SessionData(response)

    @Test
    internal fun testPrepareSendRegistration() {
        val registration = Registration(
            "firstName", "lastName", "zipCode", "50",
            Facility.MensaAmPark, Household.GroupMultipleHH, ContactType.Mail, "contact-info"
        )
        val connection = connectionUtil.prepareSendRegistration("http://localhose", registration, sessionData)

        // test connection util writes correct form name values
        assertThat(connection.data(Input.FirstName.formName).value(), equalTo(registration.firstName))
        assertThat(connection.data(Input.LastName.formName).value(), equalTo(registration.lastName))
        assertThat(connection.data(Input.ZipCode.formName).value(), equalTo(registration.zipCode))
        assertThat(connection.data(Input.TableNumber.formName).value(), equalTo(registration.tableNumber))
        assertThat(connection.data(Facility.MensaAmPark.formName).key(), equalTo(registration.facility.formName))
        assertThat(connection.data(Household.GroupMultipleHH.formName).key(), equalTo(registration.household.formName))
        assertThat(connection.data(ContactType.Mail.formName).key(), equalTo(registration.contactType.formName))
    }
}