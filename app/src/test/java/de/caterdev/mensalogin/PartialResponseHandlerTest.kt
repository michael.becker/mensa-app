package de.caterdev.mensalogin

import de.caterdev.mensalogin.handler.PartialResponseHandler
import de.caterdev.mensalogin.model.ContactType
import de.caterdev.mensalogin.model.Facility
import de.caterdev.mensalogin.model.Household
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.not
import org.hamcrest.text.IsBlankString
import org.junit.jupiter.api.Test

class PartialResponseHandlerTest : MensaAppTest() {
    private val responseHandler = PartialResponseHandler()

    @Test
    internal fun testEvaluateDocument() {
        val document = responseHandler.evaluateDocument(getErrorResponseContent())

        assertThat(document.body().toString(), not(IsBlankString.blankOrNullString()))
    }

    @Test
    internal fun testParseResponseDocument() {
        val parsedResponse = responseHandler.parseResponseDocument(getErrorResponseContent())

        assertThat(parsedResponse.startsWith("<form"), equalTo(true))
        assertThat(parsedResponse.endsWith("</form>"), equalTo(true))
    }

    @Test
    internal fun testParseSuccessResponse() {
        val registration = responseHandler.evaluateRegistration(responseHandler.evaluateDocument(getSuccessResponseContent()))

        assertThat(registration.firstName, equalTo("VornameInput"))
        assertThat(registration.lastName, equalTo("NachnameInput"))
        assertThat(registration.zipCode, equalTo("00000"))
        assertThat(registration.facility, equalTo(Facility.MensaAmPark))
        assertThat(registration.household, equalTo(Household.GroupMultipleHH))
        assertThat(registration.contactType, equalTo(ContactType.Mail))
        assertThat(registration.contact, equalTo("mail@test.com"))
    }

    @Test
    internal fun testParseForDownloadToken() {
        val result = responseHandler.parseForDownloadToken(responseHandler.evaluateDocument(getSuccessResponseContent()))

        assertThat(result, equalTo("tabForm:fbContainer:fragebogen:j_idt6270:0:downloadAusgefuelltenFragebogen"))
    }
}