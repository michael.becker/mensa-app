package de.caterdev.mensalogin

import de.caterdev.mensalogin.model.ContactType
import de.caterdev.mensalogin.model.Facility
import de.caterdev.mensalogin.model.Household
import de.caterdev.mensalogin.model.Input
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.jsoup.helper.DataUtil
import org.jsoup.nodes.Document
import org.junit.jupiter.api.Test
import java.nio.charset.Charset

class RegistrationTest : MensaAppTest() {
    private val document: Document
        get() {
            return DataUtil.load(getInitialResponse(), Charset.defaultCharset().name(), "baseUri")
        }

    @Test
    internal fun testFormIdMappings() {
        Input.values().forEach { input ->
            // mail and phone input fields are not in the original HTML page
            if (input != Input.Mail && input != Input.Phone) {
                assertThat(getLabelValue(input.formId), equalTo(input.label))
            }
        }

        Facility.values().forEach { facility ->
            assertThat(getLabelValue(facility.formId), equalTo(facility.label))
        }

        Household.values().forEach { household ->
            assertThat(getLabelValue(household.formId), equalTo(household.label))
        }

        ContactType.values().forEach { contactType ->
            assertThat(getLabelValue(contactType.formId), equalTo(contactType.label))
        }
    }

    private fun getLabelValue(formName: String): String {
        return document.getElementsByAttributeValue("for", formName)[0].text()
    }

}