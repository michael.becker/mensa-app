package de.caterdev.mensalogin

import de.caterdev.mensalogin.handler.ErrorHandler
import de.caterdev.mensalogin.model.ContactType
import de.caterdev.mensalogin.model.Facility
import de.caterdev.mensalogin.model.Household
import de.caterdev.mensalogin.model.Input
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.junit.jupiter.api.Test


class ErrorHandlerTest : MensaAppTest() {
    private val errorHandler = ErrorHandler()

    @Test
    internal fun testHasError() {
        assertThat(errorHandler.hasError(getErrorResponseContent()), equalTo(true))
    }

    @Test
    internal fun testGetErrorFields() {
        val errors = errorHandler.getErrorFields(getErrorResponseDocument())
        val expectedSize =
            Input.values().size + Facility.values().size + Household.values().size + ContactType.values().size - 2

        assertThat(errors, not(empty()))
        assertThat(errors.size, equalTo(expectedSize))
        assertThat(errors, not(contains("mailInput")))
        assertThat(errors, not(contains("phoneInput")))
    }
}