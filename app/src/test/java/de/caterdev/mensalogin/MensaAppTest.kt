package de.caterdev.mensalogin

import org.jsoup.helper.DataUtil
import org.jsoup.nodes.Document
import java.io.InputStream
import java.nio.charset.Charset

abstract class MensaAppTest {
    fun getErrorResponseContent(): String {
        return readInputStream(getErrorResponse())
    }

    fun getErrorResponseDocument(): Document {
        return DataUtil.load(getErrorResponse(), Charsets.UTF_8.name(), "baseId")
    }

    fun getSuccessResponseContent(): String {
        return readInputStream(getSuccessResponse())
    }

    fun getInitialResponse(): InputStream {
        return javaClass.getResourceAsStream("/page-response.html")!!
    }

    private fun getErrorResponse(): InputStream {
        return javaClass.getResourceAsStream("/response-error.xml")!!
    }

    private fun getSuccessResponse(): InputStream {
        return javaClass.getResourceAsStream("/response-success.xml")!!
    }

    private fun readInputStream(inputStream: InputStream): String {
        return DataUtil.load(inputStream, Charset.defaultCharset().name(), "baseUri").toString()

    }
}